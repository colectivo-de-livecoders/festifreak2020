// Contexto de audio (context) y nodo fuente (source)
let context, source;

// Instancia de Hydra
let hydra;

// Indice de la pista actual
let currentTrackIdx = 0;

// Estado del reproductor. Tiene que empezar detenido porque Chrome no ermite
// que una página autoreproduzca audio sin interacción del usuario.
let playing = false;

// Función módulo utilizada para implementar la playlist circular
Number.prototype.mod = function (n) {
  return ((this % n) + n) % n;
};

/*
 * La variable `playlist` es una lista con objetos que tienen la información de
 * cada pista:
 *
 * - url: la URL del .mp3 que usa el reproductor, debería ser /audio/[archivo].mp3
 * - artistName: Nombre del artista, como se va a mostrar en el reproductor
 * - profileUrl: URL del artista, opcional
 * - codeUrl: URL del código de la pista, opcional. Debería ser /codeScripts/[archivo].html
 * - hydraFunction: debería ser el nombre de la función con el código de hydra para esta pista
 */
let playlist = [
  {
    url:
      "https://player.vimeo.com/external/469072573.hd.mp4?s=2d8517f4e9b3370836825d0e63b5328b6077aecd&profile_id=174",
    artistName: "Aruizde",
    hydraFunction: hydraDefault,
  },
  {
    url:
      "https://player.vimeo.com/external/468790081.hd.mp4?s=728db09a8c810de9e3fa207a01f4a2607989e00c&profile_id=174",
    artistName: "Umachinita",
    hydraFunction: hydraDefault,
  },
  {
    url:
      "https://player.vimeo.com/external/468790042.hd.mp4?s=c86ad2ec9421baf490cc420efb1399c22c27c421&profile_id=174",
    artistName: "Torotumbo",
    hydraFunction: hydraDefault,
  },
  {
    url:
      "https://player.vimeo.com/external/468790028.hd.mp4?s=0598a0fe2cea61cfe5f9f663b34e43f028ac020c&profile_id=175",
    artistName: "Rafrobeat",
    hydraFunction: hydraDefault,
  },
  {
    url:
      "https://player.vimeo.com/external/468789990.hd.mp4?s=11ea7f7bd672e88cd98d4f19bc452e60f99f030e&profile_id=174",
    artistName: "irisS",
    hydraFunction: hydraDefault,
  },
  {
    url:
      "https://player.vimeo.com/external/468789945.hd.mp4?s=1c6487d78016d447a86150f4b9b07338b0112f2d&profile_id=175",
    artistName: "Hacs-Art",
    hydraFunction: hydraDefault,
  },
  {
    url:
      "https://player.vimeo.com/external/469981447.hd.mp4?s=d14d3cea1df1e9f43c665bd00bdba7a4cfcb07b5&profile_id=174",
    artistName: "Incidence",
    hydraFunction: hydraDefault,
  },
  {
    url:
      "https://player.vimeo.com/external/470014893.sd.mp4?s=9102a6d96df94b8564f676ac85541124fab7af57&profile_id=164",
    artistName: "Lowpoly",
    hydraFunction: hydraDefault,
  },
  {
    url:
      "https://player.vimeo.com/external/471161132.sd.mp4?s=4d412eacbb0cf028da0048a6f5e0ad076a275c00&profile_id=164",
    artistName: "Aruizde",
    hydraFunction: hydraDefault,
  },
  {
    url:
      "https://player.vimeo.com/external/471178686.hd.mp4?s=783a4ad4e37ae6a0948e4f4688938ce565e607dd&profile_id=175",
    artistName: "Flordefuego",
    hydraFunction: hydraDefault,
  },
];

/*
 * Reproduce pista actual, indicada por currentTrackIdx
 *
 * También inicializa el contexto de audio por primera vez.
 */
function play() {
  initializeContext();

  // Start playing
  playing = true;
  updateTrack();

  // Update icons
  const icon = document.getElementById("playIcon");
  icon.classList.add("fa-stop-circle");
  icon.classList.remove("fa-play-circle");

  // Show next/prev buttons now
  document.getElementById("prev").classList.remove("hidden");
  document.getElementById("next").classList.remove("hidden");
}

/*
 * Ejecuta la función de Hydra de la pista
 */
function runHydra() {
  // Clear hydra first
  hush();
  // Run hydra code from current track
  const track = playlist[currentTrackIdx];
  track.hydraFunction();
}

/*
 * Actualiza los datos de la pista en el reproductor, y se asegura de ejecutar
 * el reproductor y actualizar el código de Hydra
 */
function updateTrack() {
  const track = playlist[currentTrackIdx];

  const title = track.artistName;
  console.log("Current track:", title);

  const videoEl = document.getElementById("video");
  videoEl.src = track.url;

  if (playing) {
    videoEl.play();
    runHydra();
  }

  // Update artist name
  const artistNameEl = document.getElementById("artistName");
  artistNameEl.innerText = track.artistName;
  artistNameEl.setAttribute("href", track.profileUrl || "#");
}

/*
 * Reproduce o pausa dependiendo del estado
 */
function playPause() {
  if (playing) {
    pause();
  } else {
    play();
  }
}

/*
 * Pausa la reproducción
 */
function pause() {
  const videoEl = document.getElementById("video");
  videoEl.pause();
  playing = false;

  // Update icons
  var button = document.getElementById("playIcon");
  button.classList.add("fa-play-circle");
  button.classList.remove("fa-stop-circle");
}

/*
 * Avanza a la siguiente pista
 *
 * Usa la función mod (módulo) para hacer una lista circular. Es decir, si
 * está en la última pista, vuelve a la primera.
 */
function next() {
  currentTrackIdx = (currentTrackIdx + 1).mod(playlist.length);
  updateTrack();
}

/*
 * Vuelve a la anterior pista
 *
 * Usa la función mod (módulo) para hacer una lista circular. Es decir, si
 * está en la primer pista, va a la última.
 */
function previous() {
  currentTrackIdx = (currentTrackIdx - 1).mod(playlist.length);
  updateTrack();
}

/*
 * Inicializa Hydra y ajusta el canvas.
 *
 * Se ejecuta al cargar la página (evento onload)
 */
function init() {
  // Set canvas size to full screen
  var canvas = document.getElementById("canvas");
  canvas.width = window.innerWidth * window.devicePixelRatio;
  canvas.height = window.innerHeight * window.devicePixelRatio;

  // Create a new hydra-synth instance with audio disabled
  hydra = new window.Hydra({
    canvas: document.getElementById("canvas"),
    detectAudio: false,
  });

  // Shuffle playlist!
  playlist = shuffle(playlist);

  // Make container visible
  const container = document.getElementById("interactionContainer");
  container.classList.remove("hidden");

  initFade();
}

function initFade() {
  var timer;
  var fadeInBuffer = false;

  document.addEventListener("mousemove", function () {
    if (!fadeInBuffer) {
        if (timer) {
            clearTimeout(timer);
            timer = 0;
        }
        var list = document.getElementsByClassName('fade-object');
        for (let item of list) {
          item.classList.add("active")
        }
        document.documentElement.style.cursor = '';
    } else {
        fadeInBuffer = false;
    }


    timer = setTimeout(function () {
        if (!playing) return;
        var list = document.getElementsByClassName('fade-object');
        for (let item of list) {
          item.classList.remove("active")
        }
        document.documentElement.style.cursor = 'none';
        fadeInBuffer = true;
    }, 2000)
  })
}

/*
 * Inicializa el contexto de audio del <video> y fuente para Hydra
 *
 * También setea eventos generales del <video>.
 */
function initializeContext() {
  // Initialize audio context and source, if it's the first time
  if (!context || !source) {
    context = new AudioContext();

    const videoEl = document.getElementById("video");
    source = context.createMediaElementSource(videoEl);
    source.connect(context.destination);

    // Go to next track when current track finishes
    videoEl.addEventListener("ended", (event) => {
      next();
    });

    videoEl.addEventListener("waiting", () => {
      const icon = document.getElementById("playIcon");
      icon.classList.remove("fa-play-circle", "fa-stop-circle");
      icon.classList.add("fa-spinner", "fa-spin");
    });

    videoEl.addEventListener("playing", () => {
      const icon = document.getElementById("playIcon");
      icon.classList.remove("fa-spinner", "fa-spin");
      if (playing) {
        icon.classList.add("fa-stop-circle");
      } else {
        icon.classList.add("fa-play-circle");
      }
    });

    // Initialize custom audio. This CustomAudio is similar to Audio from hydra,
    // but receives an audio context and source, for audio analysis, instead of
    // the microphone.
    hydra.synth.a = new CustomAudio({
      context: context,
      source: source,
    });
    window.a = hydra.synth.a; // set global variable for audio
    hydra.detectAudio = true; // now force hydra to tick audio too
  }
}

/*
 * Devuelve una copia de array mezclada.
 */
function shuffle(array) {
  var i, j, x;
  for (i = array.length - 1; i > 0; i--) {
    j = Math.floor(Math.random() * (i + 1));
    x = array[i];
    array[i] = array[j];
    array[j] = x;
  }
  return array;
}

// Cuando se cargue la página, ejecuta la función `init`
window.addEventListener("load", init, false);
